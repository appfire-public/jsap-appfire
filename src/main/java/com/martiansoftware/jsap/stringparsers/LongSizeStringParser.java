/*
 * Copyright (c) 2002-2004, Martian Software, Inc.
 * This file is made available under the LGPL as described in the accompanying
 * LICENSE.TXT file.
 */

package com.martiansoftware.jsap.stringparsers;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.martiansoftware.jsap.ParseException;
import com.martiansoftware.jsap.StringParser;

/**
 * A {@link com.martiansoftware.jsap.StringParser} that lets the user specify sizes with an optional unit.
 * <P>
 * This parser will parse its argument using {@link #parseSize(CharSequence)}.
 *
 * @author Sebastiano Vigna
 * @see com.martiansoftware.jsap.stringparsers.IntSizeStringParser
 */

public class LongSizeStringParser extends StringParser {

    /** The regular expression used to parse the size specification. */
    private static final Pattern PARSE_SIZE_REGEX = Pattern.compile("((#|0x|0X)?[0-9A-Fa-f]+)([KMGTP]i?)?");

    /** The big integer used to check that the result is smaller than {@link Long#MAX_VALUE}. */
    private static final BigInteger LONG_MAX_VALUE = new BigInteger(Long.toString(Long.MAX_VALUE));

    /** A map from units to their size. */
    private static final HashMap<String, Long> UNIT2SIZE = new HashMap<String, Long>();

    /** The only instance of this parser. Aliased to <code>JSAP.LONG_SIZE_PARSER</code>. */
    final static LongSizeStringParser INSTANCE = new LongSizeStringParser();

    private LongSizeStringParser() {
    }

    /**
     * Returns the only instance of a {@link LongSizeStringParser}.
     * <p>
     * Convenient access to the only instance returned by this method is available through {@link com.martiansoftware.jsap.JSAP#LONGSIZE_PARSER}.
     *
     * @return the only instance of a {@link LongSizeStringParser}.
     */
    public static LongSizeStringParser getParser() {
        return INSTANCE;
    }

    @Override
    public Long parse(String arg) throws ParseException {
        return Long.valueOf(parseSize(arg));
    }

    static {
        UNIT2SIZE.put("K", Long.valueOf((long) 1E3));
        UNIT2SIZE.put("M", Long.valueOf((long) 1E6));
        UNIT2SIZE.put("G", Long.valueOf((long) 1E9));
        UNIT2SIZE.put("T", Long.valueOf((long) 1E12));
        UNIT2SIZE.put("P", Long.valueOf((long) 1E15));
        UNIT2SIZE.put("Ki", Long.valueOf(1L << 10));
        UNIT2SIZE.put("Mi", Long.valueOf(1L << 20));
        UNIT2SIZE.put("Gi", Long.valueOf(1L << 30));
        UNIT2SIZE.put("Ti", Long.valueOf(1L << 40));
        UNIT2SIZE.put("Pi", Long.valueOf(1L << 50));
    }

    /**
     * Parses a size specified using units (e.g., K, Ki, M, Mi,&hellip;).
     * <P>
     * The argument must be in the form <var>number</var> [<var>unit</var>] (with no space inbetween). <var>number</var> is anything accepted by
     * {@link Long#decode(java.lang.String)}, which allows, besides decimal numbers, hexadecimal numbers prefixed by <code>0x</code>, <code>0X</code> or
     * <code>#</code>, and octal numbers prefixed by <code>0</code>. <var>unit</var> may be one of K (10<sup>3</sup>), Ki (2<sup>10</sup>), M (10<sup>6</sup>),
     * Mi (2<sup>20</sup>), G (10<sup>9</sup>), Gi (2<sup>30</sup>), T (10<sup>12</sup>), Ti (2<sup>40</sup>), P (10<sup>15</sup>), Pi (2<sup>50</sup>). Thus,
     * for instance, 1Ki is 1024, whereas 9M is nine millions and <code>0x10Pi</code> is 18014398509481984. Note that in the number part case does not matter,
     * but in the unit part it does.
     *
     * @param s a size specified as above.
     * @return the corresponding unitless size.
     * @throws ParseException if <code>s</code> is malformed, <var>number</var> is negative or the resulting size is larger than {@link Long#MAX_VALUE}.
     */

    public static long parseSize(final CharSequence s) throws ParseException {
        final Matcher m = PARSE_SIZE_REGEX.matcher(s);
        if (!m.matches())
            throw new ParseException("Invalid size specification '" + s + "'.");

        final String unit = m.group(3);

        BigInteger unitSize = BigInteger.ONE;

        if (unit != null) {
            Long unitSizeObj = UNIT2SIZE.get(unit);
            if (unitSizeObj == null)
                throw new ParseException("Invalid unit specification '" + unit + "'.");
            unitSize = new BigInteger(unitSizeObj.toString());
        }

        final String number = m.group(1);
        final Long size;

        try {
            size = Long.decode(number);
            if (size.longValue() < 0)
                throw new ParseException("Sizes cannot be negative.");
        } catch (NumberFormatException e) {
            throw new ParseException("Invalid number '" + number + "'.");
        }

        BigInteger result = new BigInteger(size.toString()).multiply(unitSize);

        if (result.compareTo(LONG_MAX_VALUE) > 0)
            throw new ParseException("Size '" + s + "' is too big.");

        return Long.parseLong(result.toString());
    }
}
