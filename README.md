# JSAP - Java Simple Argument Parser #

* JSAP was developed by Martian Software, Inc.
* [JSAP web site](http://www.martiansoftware.com/jsap/)
* Thanks to the original authors including [Michael Lamb](https://github.com/mlamb)

### What is this repository for? ###

* This is a modified copy of the original source for JSAP 2.1
* Modified by Appfire Technologies, LLC. for distribution with Appire products
* Publicly available per the license terms via this repository
* These modifications are provided as is without support under the original license terms

### Original Copyright ###

JSAP - Java Simple Argument Parser
Copyright (c) 2002-2004, Martian Software, Inc.

### License ###
JSAP is licensed under the Lesser GNU Public License.
A copy of this license is available at
[https://www.gnu.org/licenses/license-list.html#LGPL](https://www.gnu.org/licenses/license-list.html#LGPL)

Alternate licensing terms may be obtained by contacting
the author: [http://www.martiansoftware.com/](http://www.martiansoftware.com/)

### Java LGPL Clarification ###

JSAP is Free Software.  The LGPL license is sufficiently
flexible to allow the use of JSAP in both open source
and commercial projects.  Using JSAP (by importing JSAP's
public interfaces in your Java code), and extending JSAP
(by subclassing) are considered by the authors of JSAP
to be dynamic linking.  Hence our interpretation of the
LGPL is that the use of the unmodified JSAP source or
binary, or the rebundling of unmodified JSAP classes into
your program's .jar file, does not affect the license of
your application code.

If you modify JSAP and redistribute your modifications,
the LGPL applies.

(based upon similar clarification at [http://hibernate.org/community/license/](http://hibernate.org/community/license/) )

### Modifications by Appfire ###
#### 2.2.0 ####
* Minor update for Java 1.8 - use LinkedHashMap instead of HashMap for allExceptions at line 72 of JSAPResult to consistently maintain insertion order
* Remove XML configuration support - simplification as it is not needed for our use case
* Replace Ant build with Gradle build that can build jar entirely from source repository using Gradle Wrapper
* Code directory reorganized to follow modern Gradle standards
* Code formatting (Eclipse and VSCode/Redhat Eclipse)
* Update to eliminate warnings with Java 1.8 standards

#### 2.3.0 ####
* Update to Java 11 standards changing or removing deprecated support
* Update to Gradle 7.3.1 for testing with Java 17
* Add a pipelines build

### Contribution guidelines ###

* There are no plans for public access for contributions to this repository
* Clone the repository for your own use
* No ongoing support will be provided
